#!/bin/ksh

ScriptName=AuditStrUn
PIDPath=/var/run
Log=/var/log/syslog/$ScriptName.log
Script=/usr/sbin/$ScriptName.sh

case "$1" in
  start|Start|START)
	if [ -f $PIDPath/$ScriptName.pid ]; then
		:
	else
		echo "DD" > $PIDPath/$ScriptName.pid
	fi

	if kill -0 `cat $PIDPath/$ScriptName.pid` > /dev/null 2>&1; then
		echo "$ScriptName run already with PID `cat $PIDPath/$ScriptName.pid`"
		exit 1
	fi

	nohup $Script "FORCE" $PIDPath $ScriptName >> $Log &
	sleep 2

	if kill -0 `cat $PIDPath/$ScriptName.pid` > /dev/null 2>&1; then
		echo "$ScriptName started successfully with PID `cat $PIDPath/$ScriptName.pid`"
		echo `date` " - Start $ScriptName" > $Log
	else
		echo "$ScriptName has not been started. Check permissions."
		echo `date` " - Start $ScriptName" > $Log
	fi

	exit 1
	;;

  stop|Stop|STOP)
	if kill -0 `cat $PIDPath/$ScriptName.pid` > /dev/null 2>&1; then
		kill `cat $PIDPath/$ScriptName.pid`
	else
		echo "$ScriptName not start."
		exit 1
	fi


	sleep 2
	if kill -0 `cat $PIDPath/$ScriptName.pid` > /dev/null 2>&1; then
		echo "$ScriptName still Run with PID `cat $PIDPath/$ScriptName.pid`. Check permissions."
	else
		echo "$ScriptName stopped successfully"
	fi

	exit 1
	;;
  status|Status|STATUS|stat|Stat|STAT)
	if kill -0 `cat $PIDPath/$ScriptName.pid` > /dev/null 2>&1; then
		echo "$ScriptName star with PID `cat $PIDPath/$ScriptName.pid`"
	else
		echo "$ScriptName not run"
	fi
	;;
 *)
	echo "Err. Use:"
	echo "splAud <command>"
	echo "<command>:"
	echo "    start - for start $ScriptName"
	echo "    stop - for stop $ScriptName"
	echo "    status - for check status $ScriptName"

	exit 1
	;;
esac





