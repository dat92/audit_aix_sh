#!/bin/ksh

red=$(tput setf 4)
green=$(tput setf 2)
reset=$(tput sgr0)
toend=$(tput hpa $(tput cols))$(tput cub 6)

showStat()
{
	if [ $? -eq 0 ]; then
		echo "${green}$1${toend}[OK]"
	else
		echo "${red}$1${toend}[FAIL]"
	fi
	echo "${reset}"
}

echo "${reset}\n"

if [ "$1" == "Refresh" ] || [ "$1" == "refresh" ]; then
	refresh -s syslogd
	showStat "Refresh config of syslogd..."
	exit 0
fi

audit shutdown
showStat "Shutdown audit..."

stopsrc -s syslogd
showStat "Shutdown syslogd..."

if [ "$1" == "R" ] || [ "$1" == "r" ] ; then
	sleep 5
	startsrc -s syslogd
	showStat "Start syslogd..."
	
	audit start
	showStat "Start Audit..."
fi
exit 0

