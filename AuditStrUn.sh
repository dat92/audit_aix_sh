#!/bin/ksh

StrOut=/audit/stream.out

if [ "$1" != "FORCE" ]; then
	echo "Err: use AuditSplunkStart command for start and AuditSplunkStop for stop"
	exit 1
fi

echo $$ > $2/$3.pid

while [ 1 ];
do
	B=$A
	A=`csum -h MD5 $StrOut`

	if [ "$A" != "$B" ]; then
		sed -e :a -e '$!N;s/\n / /;ta' -e 'P;D' $StrOut | sed '$!N; /^\(.*\)\n\1$/!P; D' | /usr/bin/logger -t auditd -p local0.info
		> $StrOut
	fi
	sleep 1
done
